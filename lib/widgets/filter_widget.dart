import 'package:exercise_app/controller/filter_controller.dart';
import 'package:exercise_app/models/items_filter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class FilterCategoriesWidget extends StatelessWidget {
  final bool isSelected;
  final ItemsFilter filter;

  const FilterCategoriesWidget(
      {Key? key, required this.isSelected, required this.filter})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<FilterController>(builder: (controller) {
      return Center(
        child: Container(
          margin: const EdgeInsets.only(left: 15),
          height: 45,
          padding: const EdgeInsets.only(left: 20, right: 20),
          decoration: BoxDecoration(
              color: isSelected ? Colors.black : Colors.white,
              borderRadius: BorderRadius.circular(25),
            border: Border.all(width: 1, color: Colors.grey)
          ),
          child: Center(
            child: Text(filter.name,
                style: TextStyle(
                    color: isSelected ? Colors.white : Colors.black,
                    fontSize: 20,
                    fontFamily: 'Arial',
                    fontWeight: FontWeight.w900)),
          ),
        ),
      );
    });
  }
}
