class ItemModel {
  String itemImage;
  String itemName;
  double itemPrice;

  ItemModel(this.itemImage, this.itemName, this.itemPrice);
}
