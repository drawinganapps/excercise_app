import 'package:flutter/material.dart';
import 'DarkTheme.dart';
import 'LigthTheme.dart';

class AppTheme {
  static ThemeData light = lightTheme;
  static ThemeData dark = darkTheme;
}
