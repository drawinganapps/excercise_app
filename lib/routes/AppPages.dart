import 'package:exercise_app/controller/filter_binding.dart';
import 'package:get/get.dart';
import '../screens/home_screen.dart';
import 'AppRoutes.dart';

class AppPages {
  static var list = [
    GetPage(
        name: AppRoutes.DASHBOARD,
        page: () => const HomeScreen(),
        binding: FilterBinding(),
        transition: Transition.rightToLeft)
  ];
}
