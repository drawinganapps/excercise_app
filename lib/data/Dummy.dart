import 'package:exercise_app/models/items_filter.dart';

class Dummy {

  static List<ItemsFilter> filterList = <ItemsFilter>[
    const ItemsFilter('Full body', 'FULL'),
    const ItemsFilter('Cardio', 'CARDIO'),
    const ItemsFilter('Cross fit', 'CROSS'),
    const ItemsFilter('Speed skaters', 'SPEED'),
  ];
}
