import 'package:badges/badges.dart';
import 'package:exercise_app/controller/filter_controller.dart';
import 'package:exercise_app/data/Dummy.dart';
import 'package:exercise_app/models/items_filter.dart';
import 'package:exercise_app/widgets/filter_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<FilterController>(builder: (controller) {
      List<ItemsFilter> filters = Dummy.filterList;
      return Scaffold(
          body: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                padding: const EdgeInsets.only(
                    top: 20, left: 15, right: 15, bottom: 30),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(50),
                            ),
                            clipBehavior: Clip.antiAlias,
                            child: Image.asset('assets/img/mellisa.jpg', width: 50, height: 50),
                          margin: EdgeInsets.only(right: 10),
                        ),
                        const Text('Hello, ', style: TextStyle(
                          fontWeight: FontWeight.w600,
                          fontSize: 16
                        )),
                        const Text('Melissa', style: TextStyle(
                          fontSize: 25,
                          fontWeight: FontWeight.w900,
                          fontFamily: 'sans serif'
                        )),
                      ],
                    ),
                    Container(
                      padding: const EdgeInsets.all(5),
                      child: Badge(
                        position: const BadgePosition(top: 5, end: 5),
                        badgeColor: Colors.pinkAccent,
                        child: const Icon(
                          Icons.notifications,
                          size: 30,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                  height: 50,
                  margin: const EdgeInsets.only(bottom: 20),
                  child: ListView(
                      scrollDirection: Axis.horizontal,
                      children: List.generate(filters.length, (index) {
                        return GestureDetector(
                          child: FilterCategoriesWidget(
                              isSelected: index == controller.selectedFilter,
                              filter: filters[index]),
                          onTap: () {
                            controller.changeFilter(index);
                          },
                        );
                      }))),
              Container(
                margin: const EdgeInsets.only(bottom: 30, left: 15, right: 15),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(30),
                    color: Colors.deepPurpleAccent.withOpacity(0.1)),
                padding: const EdgeInsets.only(
                    left: 15, right: 15, top: 20, bottom: 20),
                child: Column(
                  children: [
                    Row(
                      children: [
                        const Flexible(
                            child: Text(
                          'Loose booty fat',
                          style: TextStyle(
                              fontWeight: FontWeight.w900, fontSize: 32),
                        )),
                        ElevatedButton(
                          onPressed: () {},
                          style: ButtonStyle(
                            padding:
                                MaterialStateProperty.all<EdgeInsetsGeometry>(
                                    const EdgeInsets.only(
                                        top: 10,
                                        bottom: 10,
                                        left: 10,
                                        right: 10)),
                            shape: MaterialStateProperty.all<
                                RoundedRectangleBorder>(RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20),
                            )),
                            backgroundColor: MaterialStateProperty.all<Color>(
                                Colors.deepPurpleAccent.withOpacity(0.6)),
                          ),
                          child: Text('Middle level',
                              style: TextStyle(
                                  fontSize: 20,
                                  fontWeight: FontWeight.w700,
                                  color: Colors.white.withOpacity(0.9))),
                        )
                      ],
                    ),
                    Container(
                      margin: const EdgeInsets.only(top: 20, bottom: 20),
                      padding: const EdgeInsets.only(right: 50),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(25)),
                      height: 165,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Image.asset(
                            'assets/img/dumbbell.png',
                            height: 140,
                            width: 140,
                          )
                        ],
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            const Icon(Icons.watch_later),
                            Container(
                              margin: const EdgeInsets.only(left: 10),
                              child: const Text('40 Minutes',
                                  style: TextStyle(
                                      fontWeight: FontWeight.w600,
                                      fontSize: 18)),
                            )
                          ],
                        ),
                        Row(
                          children: [
                            Container(
                              margin: const EdgeInsets.only(right: 10),
                              child: const Text('Start',
                                  style: TextStyle(
                                      fontWeight: FontWeight.w900,
                                      fontSize: 20)),
                            ),
                            const Icon(Icons.keyboard_arrow_right, size: 25),
                          ],
                        )
                      ],
                    )
                  ],
                ),
              ),
              Container(
                decoration: BoxDecoration(
                  color: Colors.orange.withOpacity(0.2),
                  borderRadius: BorderRadius.circular(25),
                ),
                margin: const EdgeInsets.only(left: 15, right: 15),
                padding: const EdgeInsets.all(10),
                height: 120,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Center(
                      child: CircularPercentIndicator(
                        radius: 40.0,
                        lineWidth: 10.0,
                        animation: true,
                        percent: 0.56,
                        center: const Text(
                          "56%",
                          style: TextStyle(
                              fontWeight: FontWeight.w900, fontSize: 20.0),
                        ),
                        circularStrokeCap: CircularStrokeCap.butt,
                        progressColor: Colors.black,
                        backgroundColor: Colors.white,
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.only(left: 30),
                    ),
                    Expanded(
                        child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const Text('Great!',
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.w900,
                                fontSize: 26)),
                        Flexible(
                          child: Text(
                            "You've lost 70% of your daily calorie intake",
                            style: TextStyle(
                                fontSize: 18,
                                color: Colors.black.withOpacity(0.8),
                                fontWeight: FontWeight.w400),
                          ),
                        )
                      ],
                    ))
                  ],
                ),
              ),
            ],
          ),
          bottomNavigationBar: Container(
            decoration: const BoxDecoration(
              color: Colors.black,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(30),
                topRight: Radius.circular(30),
              ),
            ),
            height: 80,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                IconButton(
                  enableFeedback: false,
                  onPressed: () {},
                  icon: const Icon(
                    Icons.person,
                    color: Colors.white,
                    size: 30,
                  ),
                ),
                IconButton(
                  onPressed: () {},
                  icon: Icon(
                    Icons.grid_view_rounded,
                    color: Colors.white.withOpacity(0.5),
                    size: 30,
                  ),
                ),
                IconButton(
                  enableFeedback: false,
                  onPressed: () {},
                  icon: Icon(
                    Icons.tune_rounded,
                    color: Colors.white.withOpacity(0.5),
                    size: 30,
                  ),
                ),
                IconButton(
                  enableFeedback: false,
                  onPressed: () {},
                  icon: Icon(
                    Icons.event_rounded,
                    color: Colors.white.withOpacity(0.5),
                    size: 30,
                  ),
                ),
              ],
            ),
          ));
    });
  }
}
